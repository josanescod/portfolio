import os

from flask import (Flask, render_template, g, redirect, url_for)
from portfolio.db import get_db


def create_app(test_config=None):

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='test',
        DATABASE=os.path.join(app.instance_path, 'portfolio.sqlite'),

    )

    if test_config is None:

        app.config.from_pyfile('config.py', silent=True)
    else:

        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    def index():
        db = get_db()
        info = db.execute(
            'SELECT *'
            ' FROM user ').fetchone()

        return render_template('/home/home.html', info=info)

    @app.route('/contact')
    def contact():

        db = get_db()
        avatar = db.execute(
            'SELECT avatar'
            ' FROM user '
        ).fetchone()
        info = db.execute(
            'SELECT *'
            ' FROM user ').fetchone()

        return render_template('/contact/contact.html', avatar=avatar, info=info)

    from . import db
    db.init_app(app)
    from . import admin
    app.register_blueprint(admin.bp)

    from . import portfolio
    app.register_blueprint(portfolio.bp)

    return app

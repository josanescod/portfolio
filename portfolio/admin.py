import functools
import os
import shutil
import time
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from portfolio.db import get_db

bp = Blueprint('admin', __name__, url_prefix='/admin')

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('admin.login'))

        return view(**kwargs)

    return wrapped_view

@bp.route('/', methods=('GET', 'POST'))
def admin():
    db = get_db()
    
    numadmins = db.execute (
            'SELECT COUNT(*) '
            'FROM user'
           ).fetchone()
    print('numero de usuarios',numadmins[0])
    if g.user:
        return redirect(url_for('admin.panel'))
          
    elif numadmins[0] == 1: 
            return redirect(url_for('admin.login'))
        
    elif numadmins[0]==0:
        
            return redirect(url_for('admin.register'))
        
    
    

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if g.user:
        
        return redirect(url_for('admin.panel'))
    db=get_db()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    if info:
        
        
        return redirect(url_for('admin.login'))
    else:
        print ('no hay registro hay que entrar desde /admin y registrarse')
        
        
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        if not username:
            error = 'Introducir nombre de usuario.'
        elif not password:
            error = 'Introducir password.'
        elif db.execute(
            'SELECT id FROM user WHERE username = ?', (username,)
        ).fetchone() is not None:
            error = 'User {} is already registered.'.format(username)

        if error is None:
            
            db.execute(
                'INSERT INTO user (username, password,top_title,long_name,comment1,comment2,comment3,mail,linkedin,bitbucket,github,cert)'
                 'VALUES (?, ?,?,?,?,?,?,?,?,?,?,?)',
                (username, generate_password_hash(password),"","","","","","","","","","")
            )
            db.commit()
            return redirect(url_for('admin.login'))

        flash(error)

    return render_template('admin/register.html',info=info)

@bp.route('/login', methods=('GET', 'POST'))
def login():
    db = get_db()
    
    numadmins = db.execute (
            'SELECT COUNT(*) '
            'FROM user'
           ).fetchone()
    
    
    if numadmins[0]==0:
            print('login y numero de usuarios',numadmins[0])
            return redirect(url_for('admin.register'))
    if g.user:
        print ('sigues logeado')
        return redirect(url_for('admin.panel'))
    db=get_db()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    
    if request.method == 'POST':
        
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        
        user = db.execute(
            'SELECT * FROM user WHERE username = ?', (username,)
        ).fetchone()
        
      
        if user is None:
            error = 'Nombre de usuario incorrecto.'
            
        elif not check_password_hash(user['password'], password):
            error = 'Password incorrecto.'
            

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            
            return redirect(url_for('admin.panel'))

        flash(error)
    
    
    return render_template('admin/login.html',info=info)




@bp.route('/panel')
@login_required
def panel():
    db = get_db()
    
    usuario = db.execute(
            'SELECT username'
            ' FROM user ').fetchone()
    numposts = db.execute(
            'SELECT COUNT (title)'
            'FROM post').fetchone()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    print (usuario[0],numposts[0])
    return render_template('admin/admin.html',usuario=usuario,numposts=numposts,info=info)


@bp.route('/perfil', methods=('GET', 'POST'))
@login_required
def perfil():
    
    id = session.get('user_id')
    print (id)
    db = get_db()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    post = db.execute(
        'SELECT * FROM user ').fetchone()
    
    if request.method == 'POST':
        
        
        avatar = request.files['avatar'] #importante me daba error porque es request.files !!
        top_title = request.form['top_title']
        long_name = request.form['long_name']
        comment1 = request.form['comment1']
        comment2 = request.form['comment2']
        comment3 = request.form['comment3']
        mail = request.form['mail']
        linkedin = request.form['linkedin']
        bitbucket = request.form['bitbucket']
        github = request.form['github']
        cert = request.form['cert']
       # error = None
      
        db = get_db()
        # primero buscar imagen anterior
        nombreAnterior = db.execute(
        'SELECT avatar FROM user WHERE id= ?',[id]).fetchall()
        # eliminar fichero jpg anterior 
        try:
            APP_ROOT = os.path.dirname(os.path.abspath(__file__))
            target = os.path.join(APP_ROOT, 'static/images/img')
            nombreAnterior = nombreAnterior[0][0]
            print ('eliminando: '+ nombreAnterior)
            os.remove(os.path.join(
             target, nombreAnterior))
               
        except:
            print('no hay imagen para eliminar')
        
        #actualizamos la nueva informacion en la bdd
        db.execute(
           'UPDATE user SET avatar = ?, top_title = ?,long_name = ?, comment1 = ?, comment2 = ?,comment3 = ?,mail = ?, linkedin = ?, bitbucket = ?, github = ?, cert = ? '
           ' WHERE id = ?',
            (avatar.filename, top_title, long_name, comment1, comment2, comment3, mail, linkedin, bitbucket, github, cert, id)
            )
        db.commit()
            # guardando nuevo fichero jpg del post
        try:
            APP_ROOT = os.path.dirname(os.path.abspath(__file__))
            target = os.path.join(APP_ROOT, 'static/images/img')
            filename = avatar.filename
            print (target)
            print (avatar.filename)
            destination = "/".join([target,filename])
            avatar.save(destination)
               
        except:
            print('no hay imagen para guardar')
           
        print ('datos actualizados')
        flash('Datos actualizados')
        return redirect(url_for('admin.perfil'))
    
    print (post ['top_title'] )
    
    return render_template('admin/perfil.html', post = post,info=info)

@bp.route('/password', methods=('GET', 'POST'))
@login_required
def password():
    
    id = session.get('user_id')
    print (id)
    db = get_db()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    
    
    if request.method == 'POST':
        
        password = request.form['password']
        
       # error = None
      
        db = get_db()
               
        #actualizamos la nueva informacion en la bdd
        db.execute(
           'UPDATE user SET password = ? '
           ' WHERE id = ?',
            (generate_password_hash(password), id)
            )
        db.commit()
        
        
        return redirect(url_for('admin.logout2'))
    
    
    return render_template('admin/password.html',info=info)

# borra bdd y todos los datos
@bp.route('/reset', methods=('GET',))
@login_required
def reset():
    APP_ROOT = os.path.dirname(os.path.abspath(__file__))
    target = os.path.join(APP_ROOT, 'static/images/img')
    shutil.rmtree(target) 
    os.mkdir(target)
    db = get_db()
    db.execute(
            'DELETE FROM user')
           
    db.execute(
            'DELETE FROM post')
    
    db.commit()
    db.execute('VACUUM;')
    flash ('Base de datos inicializada')
    return redirect(url_for('admin.register'))
    

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()

@bp.route('/logout')
@login_required
def logout():
    session.clear()
    flash ('Sesion finalizada')
    return redirect(url_for('index'))

@bp.route('/logout2')
@login_required
def logout2():
    session.clear()
    flash ('Password modificado, necesitas volver a identificarte')
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('admin.login'))

        return view(**kwargs)

    return wrapped_view


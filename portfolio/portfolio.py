from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
from werkzeug.utils import secure_filename
import os
from portfolio.admin import login_required
from portfolio.db import get_db

bp = Blueprint('portfolio', __name__, url_prefix='/portfolio')


# muestra todos los proyectos en la pagina portfolio
@bp.route('/')
def index():

    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username, image'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    return render_template('portfolio/index.html', posts=posts, info=info)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    db = get_db()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        image = request.files['image']
        long_comment = request.form['long_comment']
        repositoryb = request.form['repositoryb']
        repositoryg = request.form['repositoryg']
        link = request.form['link']
        error = None

        if not title:
            error = 'Escribe un titulo.'

        if error is not None:
            flash(error)
        else:

            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, image, author_id,long_comment,repositoryb,repositoryg,link)'
                ' VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
                (title, body, image.filename, g.user['id'], long_comment, repositoryb, repositoryg, link)
            )
            db.commit()
            flash('Proyecto creado')
            # guardando fichero jpg del post
            try:
                APP_ROOT = os.path.dirname(os.path.abspath(__file__))
                target = os.path.join(APP_ROOT, 'static/images/img')
                filename = image.filename
                print(target)
                print(image.filename)
                destination = "/".join([target, filename])
                image.save(destination)

            except:
                print('no hay imagen')
            return redirect(url_for('portfolio.index'))

    return render_template('portfolio/create.html', info=info)

# para obtener el post de un usuario en este caso solo hay el usuario admin


def get_post(id, check_author=True):
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username, image, long_comment,repositoryb, repositoryg,link'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if check_author and post['author_id'] != g.user['id'] and g.user['admin'] == 1:
        return post

    if post is None:
        abort(404, "Post id {0} no existe.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post

# actualizar post
@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)
    db = get_db()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        image = request.files['image']
        long_comment = request.form['long_comment']
        repositoryb = request.form['repositoryb']
        repositoryg = request.form['repositoryg']
        link = request.form['link']
        error = None

        if not title:
            error = 'Escribe un titulo.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            # primero buscar imagen anterior
            nombreAnterior = db.execute(
                'SELECT image FROM post WHERE id= ?', [id]).fetchall()

            # eliminar fichero jpg anterior
            try:
                APP_ROOT = os.path.dirname(os.path.abspath(__file__))
                target = os.path.join(APP_ROOT, 'static/images/img')
                nombreAnterior = nombreAnterior[0][0]
                os.remove(os.path.join(
                    target, nombreAnterior))

            except:
                print('no hay imagen para borrar')

            # actualizamos la nueva informacion en la bdd
            db.execute(
                'UPDATE post SET title = ?, body = ?, image = ? , long_comment = ?, repositoryb = ?, repositoryg = ?, link = ?'
                ' WHERE id = ?',
                (title, body, image.filename, long_comment, repositoryb, repositoryg, link, id)
            )
            db.commit()
            flash('Proyecto actualizado')
            # guardando nuevo fichero jpg del post
            try:
                APP_ROOT = os.path.dirname(os.path.abspath(__file__))
                target = os.path.join(APP_ROOT, 'static/images/img')
                filename = image.filename

                destination = "/".join([target, filename])
                image.save(destination)

            except:
                print('no hay imagen para guardar')
            return redirect(url_for('portfolio.index'))

    return render_template('portfolio/update.html', post=post, info=info)


@bp.route('/<int:id>/delete', methods=('GET',))
@login_required
def delete(id):
    db = get_db()
    # primero buscar imagen anterior
    nombreAnterior = db.execute(
        'SELECT image FROM post WHERE id= ?', [id]).fetchall()

    # eliminar fichero jpg anterior
    try:
        APP_ROOT = os.path.dirname(os.path.abspath(__file__))
        target = os.path.join(APP_ROOT, 'static/images/img/')
        nombreAnterior = nombreAnterior[0][0]
        os.remove(os.path.join(
            target, nombreAnterior))

    except:
        print('no hay imagen')

    # borrar datos de la bd
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    flash('proyecto eliminado')
    return redirect(url_for('portfolio.index'))


# mostrar post ampliado
@bp.route('/project/<int:id>')
def project(id):
    db = get_db()
    info = db.execute(
        'SELECT *'
        ' FROM user ').fetchone()
    post = get_post(id, False)
    return render_template('portfolio/project.html', post=post, info=info)

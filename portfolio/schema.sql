DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS post;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  avatar TEXT,
  top_title TEXT NOT NULL,
  long_name TEXT NOT NULL,
  comment1 TEXT NOT NULL,
  comment2 TEXT NOT NULL,
  comment3 TEXT NOT NULL,
  mail TEXT NOT NULL,
  linkedin TEXT NOT NULL,
  bitbucket TEXT NOT NULL,
  github TEXT NOT NULL,
  cert TEXT NOT NULL

  

);

CREATE TABLE post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  long_comment TEXT NOT NULL,
  image TEXT NOT NULL,
  repositoryb TEXT NOT NULL,
  repositoryg TEXT NOT NULL,
  link TEXT NOT NULL,
  FOREIGN KEY (author_id) REFERENCES user (id)
);
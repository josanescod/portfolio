#!/bin/bash
#portfolio.sh
#variables
version=1.0



#numero de parametros
if [ $# -ne 1 ]; then
	clear
	echo "portfolio v$version"
	echo ""
	echo "[ERROR: se esperaba un parametro]"
	echo "Ejemplo: $0 -h "
	exit 1
fi


function check_docker(){
    systemctl status docker > /dev/null
	if [ $? -ne 0 ]; then
	clear
	echo "portfolio v$version"
	echo ""
	echo "[ERROR: se necesita docker-ce para ejecutar el script]"
	echo "https://docs.docker.com/install/"
	exit 1
	fi
}


function install(){
    echo "[INSTALANDO portfolio webapp..]"
    docker build -t 'portfolio:latest' .
    docker run -d -i -t --name portfolio_app -p 5000:8080 --restart always -v portfoliobd:/app/venv/var/portfolio-instance portfolio:latest
    clear
    docker ps -a
    echo "Instalacion completada. [OK]"
    echo "para acceder en modo administrador abrir un navegador con la url-> localhost:5000/admin"
}


function remove(){
    echo "[DESINSTALANDO portfolio webapp..]"
    docker stop portfolio_app
    docker rm portfolio_app
    docker rmi portfolio:latest
    docker rmi python:alpine3.7
    docker volume rm portfoliobd
    clear 
    docker ps -a	
    echo "Aplicacion desinstalada. [OK]"
}

function parametro_incorrecto(){
    clear
    echo "portfolio v$version"
    echo ""
    echo "[ERROR: parametro incorrecto.]"
    echo "$0 -h para mas informacion."
    exit 1
}

#funcion help
function help() {
    clear
    echo "[AYUDA]"
    echo "instalador portfolio v$version"
    echo ""
    echo "portfolio.sh [opcion]"
    echo "opciones:"
    echo "	-h ayuda."
    echo "	-i instala aplicacion"
    echo "	-r borra la aplicacion."
    echo ""
    echo "23/08/2019"
    echo ""
}

case "$1" in
    
    #instala discoverme
    -i)
    	check_docker
	install
    ;;

    #desinstala discoverme
    -r)
    	check_docker
	remove
    ;;

    #muestra ayuda
    -h)
        help
    ;;
 #opcion invalida
    *)
        parametro_incorrecto
    
    ;;
esac
exit 0

